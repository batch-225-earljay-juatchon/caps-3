import React from 'react'
import { Form, Button } from 'react-bootstrap'
import { useState, useEffect } from 'react'
import { useNavigate } from 'react-router-dom'
import Swal from 'sweetalert2'

export default function AddProduct() {

    const navigate = useNavigate()
    const [name, setName] = useState('')
    const [description, setDescription] = useState('')
    const [color, setColor] = useState('')
    const [quantity, setQuantity] = useState('')
    const [price, setPrice] = useState('')

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)


    function addProduct(event) {
        event.preventDefault()

        fetch(`${process.env.REACT_APP_API_URL}/product/addProduct`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                color: color,
                quantity: quantity,
                price: price
            })
        })
            .then(response => response.json())
            .then(result => {

                setName('');
                setDescription('');
                setColor('');
                setQuantity('');
                setPrice('');

                if (result) {
                    Swal.fire({
                        title: 'Added Products!',
                        icon: 'success',
                        text: 'Successfully!'
                    })

                    navigate('/users')
                } else {
                    Swal.fire({
                        title: 'Oops',
                        icon: 'error',
                        text: "Try again!"
                    })
                }
            })
    }

    useEffect(() => {
        if (name !== '' && description !== '' && color !== '' && quantity !== '' && price !== '') {
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, color, quantity, price])

    return (


        <div className="reg-container mt-5">
            <div className="reg-box">
                <Form className="reg-form" onSubmit={event => addProduct(event)}>
                    <h1 className="reg">Add Product</h1>
                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="enter name"
                            value={name}
                            onChange={event => setName(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="description">
                        <Form.Label>Description</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="description"
                            value={description}
                            onChange={event => setDescription(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="color">
                        <Form.Label>Color</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="color"
                            value={color}
                            onChange={event => setColor(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="quantity">
                        <Form.Label>Quantity</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="quantity"
                            value={quantity}
                            onChange={event => setQuantity(event.target.value)}
                            required
                        />
                        </Form.Group>

                        <Form.Group controlId="price">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="price"
                                value={price}
                                onChange={event => setPrice(event.target.value)}
                                required
                            />
                        </Form.Group>


                        {isActive ?
                            <Button variant="primary" type="submit" id="submitBtn">
                                Submit
                            </Button>
                            :
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                                Submit
                            </Button>
                        }

                </Form>
            </div>
        </div>
    )
}
