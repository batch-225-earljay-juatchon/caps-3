import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import { Link, NavLink } from 'react-router-dom'
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext'

export default function AppNavbar(){

	const {user} = useContext(UserContext)
	console.log(user.id);
	

	return(
		<Navbar className="nav1"  expand="lg">
			<div className='log'>
			<Navbar.Brand as={Link} to="/"><img className='logo' src='https://rcb.com/wp-content/uploads/2021/11/rcb-logo-single-line.png'/></Navbar.Brand>
			</div>
			<h4>Motor Parts and Accesories</h4>
			<Navbar.Toggle aria-controls="basic-navbar-nav" />
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={NavLink} to="/">Home</Nav.Link>
					<Nav.Link as={NavLink} to="/activeProduct">Product</Nav.Link>
					
					{	(user.id) ?
						<Nav.Link as={NavLink} to="/logout">Logout</Nav.Link>
						:
						<>
							<Nav.Link as={NavLink} to="/login">Login</Nav.Link>
							<Nav.Link as={NavLink} to="/register">Register</Nav.Link>
						</>
					}

					
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>
	
		

	
	)
}