import Button from 'react-bootstrap/Button';
import Modal from 'react-bootstrap/Modal';
import UserContext from '../UserContext'
import { useState, useEffect, useContext, setState } from 'react';
import {useParams, useNavigate} from 'react-router-dom'
import Swal from 'sweetalert2'
import Loading from '../components/Loading.js'

export default function Example() {
  const [lgShow, setLgShow] = useState(false);
  const [isLoading, setIsLoading] = useState(true)

  const {productId} = useParams()

  const {user} = useContext(UserContext)

  const navigate = useNavigate()


  const [name, setName] = useState("");
  const [price, setPrice] = useState(0);
  const [totalAmount, setTotalAmount] = useState(0);
  const [orderId, setOrderId] = useState('')



  //   useEffect(() => {
  //   fetch(`${process.env.REACT_APP_API_URL}/order/getCheckOut/${productId}`, 
  //     {
  //       headers: {
  //           'Content-Type' : 'application/json',
  //           Authorization: `Bearer ${localStorage.getItem('token')}`
  //         }
  //       })
  //   .then(response => response.json())
  //   .then( async result => {

  //     const productOrdered = await result.products
  //     const productSpecific =await productOrdered[0]
  //     const productName = await productSpecific.name;
  //     const productPrice = await productSpecific.price;

  //     console.log(result)
  //     console.log(productName)
  //     console.log(productPrice)
  //     setName(productName)
  //     setPrice(productPrice)
  //     setTotalAmount(result.totalAmount)
  //     setOrderId(result._id)
  //   })
  // }, [productId])

  const purchase = (productId) => {
    fetch(`${process.env.REACT_APP_API_URL}/order/createOrder/${productId}`, {
      method: 'POST',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      
      
      fetch(`${process.env.REACT_APP_API_URL}/order/getCheckOut/${productId}`, 
      {
        headers: {
            'Content-Type' : 'application/json',
            Authorization: `Bearer ${localStorage.getItem('token')}`
          }
        })
    .then(response => response.json())
    .then( async result => {

      const productOrdered = await result.products
      const productSpecific =await productOrdered[0]
      const productName = await productSpecific.name;
      const productPrice = await productSpecific.price;

      console.log(result)
      console.log(productName)
      console.log(productPrice)
      setName(productName)
      setPrice(productPrice)
      setTotalAmount(result.totalAmount)
      setOrderId(result._id)
    })

    if(orderId === null){
        if(result) {
        setIsLoading(false)
        setLgShow(true)
      } else {
        console.log(result)

        Swal.fire({
          title: "Something went wrong!",
          icon: "error",
          text: "Please try again :("
        })
        }
      } else {
        setLgShow(true)
      }

    })
    // We may put the error alert in a catch block
  }

const checkOut = (orderId) => {
    fetch(`${process.env.REACT_APP_API_URL}/order/checkout/${orderId}`, {
      method: 'PUT',
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem('token')}`
      }
    })
    .then(response => response.json())
    .then(result => {
      if(result) {
        Swal.fire({
          title: "Purchase Successful!",
          icon: "success",
          text: "Thank you for your purchase!"
      }) 
        navigate('/products')
      } else {
        console.log(result)

        Swal.fire({
          title: "Something went wrong!",
          icon: "error",
          text: "Please try again :("
        })
      }
    })
    // We may put the error alert in a catch block
  }


  return (
    <>
      <Button onClick={() => purchase(productId)}>Buy Now</Button>
      <Modal
        size="lg"
        show={lgShow}
        onHide={() => setLgShow(false)}
        aria-labelledby="example-modal-sizes-title-lg"
      >
      {(orderId === null && isLoading) ?
      <Modal.Body>
      <Loading/>
      </Modal.Body>
      :
      <>
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-lg">
            Order
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <h1>Order has been sent</h1>
          <div>{orderId}</div>
          <div>{name}</div>
          <div>Product Price: {price}</div>
          <div>Total Amount: {totalAmount}</div>
          <Button onClick={() => checkOut(orderId)}> Check Out </Button>
        </Modal.Body>
      </>}
      </Modal>
    </>
  );
}
