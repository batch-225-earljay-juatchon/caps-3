import React from 'react';
import { Form, Button } from 'react-bootstrap';
import { useState, useEffect } from 'react';
import { useNavigate, useParams } from 'react-router-dom';
import Swal from 'sweetalert2';


const UpdateProduct = () => {
    const { id } = useParams()
    const navigate = useNavigate();
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');
    const [price, setPrice] = useState('');
    const [quantity, setQuantity] = useState('');
    const [color, setColor] = useState('');


    const [isActive, setIsActive] = useState(false);

    const [name1, setName1] = useState('');
    const [description1, setDescription1] = useState('');
    const [price1, setPrice1] = useState('');
    const [quantity1, setQuantity1] = useState(0);
    const [color1, setColor1] = useState('');

    function updateProduct(event) {
        event.preventDefault();
        fetch(`${process.env.REACT_APP_API_URL}/product/updateProduct/${id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                name: name,
                description: description,
                price: price,
                quantity: quantity,
                color: color

            })
        })
            .then(response => response.json())
            .then(result => {
                setName('');
                setDescription('');
                setPrice('');
                setQuantity('');
                setColor('');



                if (result) {
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Product Updated Successfully',
                        showConfirmButton: false,
                        timer: 1500
                    })

                    navigate('/retrieveAllProduct')

                } else {
                    Swal.fire({
                        title: 'Oops!',
                        icon: 'error',
                        text: "Try again!"
                    })
                }
            })
    }

    useEffect(() => {
        fetch(`${process.env.REACT_APP_API_URL}/product/retrieveSingleProduct/${id}`)
            .then(response => response.json())
            .then(result => {
                setName1(result.name)
                setDescription1(result.description)
                setPrice1(result.price)
                setQuantity1(result.quantity)
                setColor1(result.color)

            })
    }, [id])

    useEffect(() => {
        if (name !== '' && description !== '' && price !== '' && quantity !== '' && color !== '') {
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [name, description, price, quantity, color])


    console.log(name1)
    return (
        <>

            <h2 className='dash-text-color pb-1 text-center mt-5'>UpdateProduct</h2>
            <div className="container-xxl center">
                <div className="row">
                    <div className='col-4'/>
                    <div className="col-4">
                        <Form className='filter-form d-flex flex-column gap-10' onSubmit={event => updateProduct(event)}>
                            <Form.Group controlId="name">
                                <Form.Label className='dash-text-color dash-text-size'><b>Product:</b> {name1}</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={name}
                                    onChange={event => setName(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="description">
                                <Form.Label className='dash-text-color dash-text-size'><b>Description:</b> {description1}</Form.Label>
                                <Form.Control
                                    as="textarea"
                                    rows={5}
                                    value={description}
                                    onChange={event => setDescription(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="price">
                                <Form.Label className='dash-text-color dash-text-size'><b>Price:</b>{price1}</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={price}
                                    onChange={event => setPrice(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="quantity">
                                <Form.Label className='dash-text-color dash-text-size'><b>Quantity:</b> {quantity1}</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={quantity}
                                    onChange={event => setQuantity(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <Form.Group controlId="color">
                                <Form.Label className='dash-text-color dash-text-size'><b>Color:</b> {color1}</Form.Label>
                                <Form.Control
                                    type="text"
                                    value={color}
                                    onChange={event => setColor(event.target.value)}
                                    className='form-control'
                                    required
                                />
                            </Form.Group>
                            <div className='d-flex justify-content-center gap-15 align-items-center'>
                            </div>
                            {isActive ?
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn">
                                    Submit
                                </Button>
                                :
                                <Button className='login-wide-button my-2' variant="success" type="submit" id="submitBtn" disabled>
                                    Submit
                                </Button>
                            }
                        </Form>
                    </div>
                </div>
            </div>

        </>
    )
}

export default UpdateProduct