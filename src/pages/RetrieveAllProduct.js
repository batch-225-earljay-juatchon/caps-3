import ProductCard from '../components/ProductCard'
import Loading from '../components/Loading'
import { useEffect, useState } from 'react'
import AdminDashbord from '../components/AdminDashbord' 
export default function RetrieveAllProducts() {
    const [product, setProduct] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    
    useEffect(() => {



        // If it detects the data coming from fetch, the set isloading going to be false
        fetch(`${process.env.REACT_APP_API_URL}/product/retrieveAllProducts`)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                setProduct(
                    result.map(product => {
                        return (
                            
                            <ProductCard key={product._id} productProp={product} />
                            
                            
                        )
                        
                    })
                )     
                    
                // Sets the loading state to false
                setIsLoading(false)
            })
    }, [])

    return (

        (isLoading) ?
            <Loading />
            :
            <>
                {product}
            </>
    )
}