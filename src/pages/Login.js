import {useState, useEffect, useContext} from 'react'
import {Form, Button, FormGroup} from 'react-bootstrap'
import {useNavigate, Navigate} from 'react-router-dom'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'
import { Container} from 'react'


export default function Login(){
    // Initializes the use of the properties from the UserProvider in App.js file
    const {user, setUser} = useContext(UserContext)

    const [email, setEmail] = useState('')
    const [password, setPassword] = useState('')

    // Initialize useNavigate
    // const navigate = useNavigate()

    // For determining if button is disabled or not
    const [isActive, setIsActive] = useState(false)

    const retrieveUser = (token) => {
        fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
        .then(response => response.json())
        .then(result => {

            // Store the user details retrieved from the token into the global user state
            setUser({
                id: result._id,
                isAdmin: result.isAdmin
            })
        })
    }

    function authenticate(event){
        event.preventDefault()
        
        fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email,
                password: password
            })
        })
        .then(response => response.json())
        .then(result => {
            if(typeof result.access !== "undefined"){
                localStorage.setItem('token', result.access)

                retrieveUser(result.access)

                
                
                

                Swal.fire({
                    title: 'Login Successful!',
                    icon: 'success',
                    text: 'Welcome to Zuitt!'
                })
            } else {
                Swal.fire({
                    title: 'Authentication Failed!',
                    icon: 'error',
                    text: 'Invalid Email or password'
                })
            }
        })
    }

    useEffect(() => {
        if((email !== '' && password !== '')){
            // Enables the submit button if the form data has been verified
            setIsActive(true)
        } else {
            setIsActive(false)
        }
    }, [email, password])
    console.log(user)
    return(
        (user.id !== null) ?
        ((user.isAdmin) ?
            <Navigate to='/adminDashbord' />
            :
            <Navigate to="/activeProduct" />
        )
        :
           
         
            <div className="log-container">
                <div className="log-box">
                    <Form className="log-form" onSubmit={event => authenticate(event)}>
                        <h1 className="log">Login</h1>
                        <Form.Group controlId="userEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control 
                                type="email" 
                                placeholder="Enter email"
                                value={email}
                                onChange={event => setEmail(event.target.value)}
                                required
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                            </Form.Text>
                        </Form.Group>

                        <Form.Group controlId="password">
                            <Form.Label>Password</Form.Label>
                            <Form.Control 
                                type="password" 
                                placeholder="Password"
                                value={password}
                                onChange={event => setPassword(event.target.value)}
                                required
                            />
                        </Form.Group>

                        {   isActive ?
                            
                            <Button
                            className="border-0 px-3 py-2 text-white fw-bold" variant="primary" type="submit" id="submitBtn">
                                Submit
                            </Button>
                            
                            :
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                                Submit
                            </Button>
                        }
                        
                    </Form>
                </div>
            </div>
         
            
    )
}
