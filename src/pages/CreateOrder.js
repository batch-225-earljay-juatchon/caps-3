import React from "react";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button } from 'react-bootstrap'

import Swal from "sweetalert2";

export default function CreateOrder () {

    const navigate = useNavigate()
    const [userId, setUserId] = useState('')
    const [productId, setproductId] = useState('')
    const [name, setname] = useState('')
    const [totalAmount, settotalAmount] = useState('')

    const [isActive, setIsActive] = useState(false)

    function CreateOrder (event) {
        event.preventDefault()


        fetch(`${process.env.REACT_APP_API_URL}/users/getUser/details`, {
            headers: {
                method: 'POST',
                // Authorization: `Bearer ${token}`
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userId: userId,
                productId: productId,
                name: name,
                totalAmount: totalAmount

            })
        })
        .then(response => response.json())
        .then(result => {

            setUserId('')
            setproductId('')
            setname('')
            settotalAmount('')

            if (result) {
                Swal.fire({
                    title: 'Added Products!',
                    icon: 'success',
                    text: 'Successfully!'
                })
                navigate('/users')
                } else {
                    Swal.fire({
                        title: 'Oops',
                        icon: 'error',
                        text: "Try again!"
                    })
            }
           
        })
    }

    useEffect(() => {
        if (userId !== '' && productId !== '' && name !== '' && totalAmount !== '') {
            
            setIsActive(true)
        }
    }, [userId, productId, name, totalAmount])
    

    return (
        <div className="reg-container mt-5">
            <div className="reg-box">
                <Form className="reg-form" onSubmit={event => CreateOrder(event)}>
                    <h1 className="reg">Create Order</h1>
                    <Form.Group controlId="name">
                        <Form.Label>userId</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="userId"
                            value={userId}
                            onChange={event => setUserId(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="productId">
                        <Form.Label>productId</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="productId"
                            value={productId}
                            onChange={event => setproductId(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="name">
                        <Form.Label>Name</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="name"
                            value={name}
                            onChange={event => setname(event.target.value)}
                            required
                        />
                    </Form.Group>

                    <Form.Group controlId="totalAmount">
                        <Form.Label>totalAmount</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="totalAmount"
                            value={totalAmount}
                            onChange={event => settotalAmount(event.target.value)}
                            required
                        />
                        </Form.Group>

                       
                        {isActive ?
                            <Button variant="primary" type="submit" id="submitBtn">
                                Submit
                            </Button>
                            :
                            <Button variant="primary" type="submit" id="submitBtn" disabled>
                                Submit
                            </Button>
                        }

                </Form>
            </div>
        </div>
    )
    
}