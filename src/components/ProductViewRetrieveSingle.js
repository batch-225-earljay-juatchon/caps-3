import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
import {useParams, useNavigate, Link} from 'react-router-dom'
import CheckOut from '../pages/OrderCheckout'
import UserContext from '../UserContext'
import Swal from 'sweetalert2'


export default function ProductViewRetrieveSingle(product) {

	// Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
	const {productId} = useParams()

	const {user} = useContext(UserContext)

	const navigate = useNavigate()


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [image, setImage] = useState("");
	const [color, setColor] = useState("");

	
	{/*<CardImg top src="https://media.npr.org/assets/img/2021/11/25/gettyimages-1351143298-e0023f4640974350830dc818ef1bb2672bb43830.jpg"/>*/}

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/product/retrieveSingleProduct/${productId}`)
		.then(response => response.json())
		.then(result => {
			console.log(result.name)
			console.log(result.price)
			console.log(result.description)
			console.log(result.image)
			console.log(result.color)
			setName(result.name)
			setDescription(result.description)
			setPrice(result.price)
			setImage(result.image)
			setColor(result.color)
		})
		

	}, [productId])
	


	return(
	<>


				<Container className="mt-5 ">
					<Row className="mt-3 mb-3">
						<Col xs={12} md={12}>
							<Card>
								<Card.Body className="text-center">
									<Card.Title>{name}</Card.Title>
									<Card.Subtitle>Description:</Card.Subtitle>
									<Card.Text>{description}</Card.Text>
									<Card.Subtitle>Price:</Card.Subtitle>
									{/* <CardImg top src={price}/> */}
									<Card.Text>Php {price}</Card.Text>
									<Card.Subtitle>Color:</Card.Subtitle>
									<Card.Text>{color}</Card.Text>
									{/* <CardImg top src={image}/> */}


									{	user.id !== null ? 
										<CheckOut/>
										
										:
										<Button variant="primary">Buy</Button>
									}
									
								</Card.Body>		
							</Card>
						</Col>
					</Row>
				</Container>
	

	</>
	)
}


// import { useState, useEffect, useContext } from 'react';
// import { Container, Card, Button, Row, Col, CardImg } from 'react-bootstrap';
// import { useParams, useNavigate, Link } from 'react-router-dom'
// import UserContext from '../UserContext'
// import Swal from 'sweetalert2'


// export default function ProductViewRetrieveSingle(product) {

//   // Gets the courseId from the URL of the route that this component is connected to. '/courses/:courseId'
//   const { productId } = useParams()

//   const { user, setUser } = useContext(UserContext)

//   const navigate = useNavigate()


//   const [name, setName] = useState("");
//   const [description, setDescription] = useState("");
//   const [price, setPrice] = useState(0);
//   const [image, setImage] = useState("");
//   const [color, setColor] = useState("");


//   useEffect(() => {
//     fetch(`${process.env.REACT_APP_API_URL}/product/retrieveSingleProduct/${productId}`)
//       .then(response => response.json())
//       .then(result => {
//         console.log(result.name)
//         console.log(result.price)
//         console.log(result.description)
//         console.log(result.image)
//         console.log(result.color)
//         setName(result.name)
//         setDescription(result.description)
//         setPrice(result.price)
//         setImage(result.image)
//         setColor(result.color)
//       })
//   }, [productId])

//   const handleBuyNow = () => {
//     fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`)
//       .then(response => response.json())
//       .then(result => {
//         const order = {
//           orderId: Date.now().toString(),
//           products: [
//             {
//               productId: productId,
//               name: name,
//               price: price,
//             }
//           ],
//           totalAmount: price,
//           purchasedOn: new Date().toISOString(),
//         }
//         const updatedUser = {
//           ...result,
//           orders: [...result.orders, order],
//         }
//         fetch(`${process.env.REACT_APP_API_URL}/users/updateUser`, {
//           method: 'PUT',
//           headers: {
//             'Content-Type': 'application/json',
//           },
//           body: JSON.stringify(updatedUser),
//         })
//           .then(response => response.json())
//           .then(result => {
//             setUser(updatedUser)
//             Swal.fire({
//               title: 'Order Placed',
//               icon: 'success',
//               text: 'Thank you for your purchase!',
//               confirmButtonText: 'OK',
//             })
//             navigate('/home')
//           })
//       })
//   }

//   return (
//     <>
//       <Container className="mt-5 ">
//         <Row className="mt-3 mb-3">
//           <Col xs={12} md={3}>
//             <Card>
//               <Card.Body className="text-center">
//                 <Card.Title>{name}</Card.Title>
//                 <Card.Subtitle>Description:</Card.Subtitle>
//                 <Card.Text>{description}</Card.Text>
//                 <Card.Subtitle>Price:</Card.Subtitle>
//                 <Card.Text>Php {price}</Card.Text>
//                 <Card.Subtitle>Color:</Card.Subtitle>
//                 <Card.Text>{color}</Card.Text>
//                 {user.id !== null ?
//                   <Button variant="primary" onClick={handleBuyNow}>Buy Now</Button>
//                   :
//                   <Link className="btn btn-primary btn-block" to="/home">Buy</Link>
//                 }
//               </Card.Body>
//             </Card>
//           </Col>
//         </Row>
//       </Container>
//     </>
//   )
// }
