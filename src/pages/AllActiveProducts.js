import ProductCard from '../components/ProductCard'
import Loading from '../components/Loading'
import { useEffect, useState } from 'react'

export default function Product() {
    const [product, setProduct] = useState([])
    const [isLoading, setIsLoading] = useState(true)
    console.log(process.env.REACT_APP_API_URL)
    useEffect((isLoading) => {



        // If it detects the data coming from fetch, the set isloading going to be false
        fetch(`${process.env.REACT_APP_API_URL}/product/retrieveAllActiveProducts`)
            .then(response => response.json())
            .then(result => {
                console.log(result)
                setProduct(
                    result.map(product => {
                        return (
                            <ProductCard key={product._id} productProp={product} />
                        )
                    })
                )

                // Sets the loading state to false
                setIsLoading(false)
            })
    }, [])

    return (

        (isLoading) ?
            <Loading />
            :
            <>
            {/* <div className='products-container'> */}
                {product}
                {/* </div> */}
            </>
    )
}