import './App.css';
import './css/style.css';
import {useState, useEffect} from 'react'
import {UserProvider} from './UserContext'
import AppNavbar from './components/AppNavbar'
import Home from './pages/Home'
import Register from './pages/Register'
import Login from './pages/Login'
import Logout from './pages/Logout';
import AllActiveProducts from './pages/AllActiveProducts'
import ProductViewRetrieveSingle from './components/ProductViewRetrieveSingle'
// import AdminLayout from './components/AdminLayout '
import AddProduct from './pages/AddProduct'
import RetrieveAllProduct from './pages/RetrieveAllProduct';
import ArchiveProduct from './pages/ArchiveProduct';
import AdminDashbord from './components/AdminDashbord';
import UpdateProduct from './pages/UpdateProduct';
import CreateOrder from './pages/CreateOrder';


import { BrowserRouter as Router, Route, Routes } from 'react-router-dom'



function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  })

  const unsetUser = () => {
    localStorage.clear()
  }

      //Because our user state's values are reset to null every time the user reloads the page (thus logging the user out), we want to use React's useEffect hook to fetch the logged-in user's details when the page is reloaded. By using the token saved in localStorage when a user logs in, we can fetch the their data from the database, and re-set the user state values back to the user's details.
      useEffect(() => {

      fetch(`${process.env.REACT_APP_API_URL}/users/userDetails`, {
        headers: {
          Authorization: `Bearer ${ localStorage.getItem('token') }`
        }
      })
      .then(res => res.json())
      .then(data => {

        // Set the user states values with the user details upon successful login.
        if (typeof data.id !== "undefined") {

          setUser({
            id: data.id,
            isAdmin: data.isAdmin
          });
        

        // Else set the user states to the initial values
        } else {

          setUser({
            id: null,
            isAdmin: null
          });

        }

      })

      }, []);


      // console.log(user)
  return (
    <>
    
      {/*Provides the user context throughout any component inside of it.*/}
      <UserProvider value={{user, setUser, unsetUser}}>
        {/*Initializes that dynamic routing will be involved*/}
        <Router>
          <AppNavbar/>
        
            <Routes>
           
              <Route path="/" element={<Home/>}/>
              <Route path="/login" element={<Login/>}/>
              <Route path="/logout" element={<Logout/>}/>
              <Route path="/register" element={<Register/>}/>
              <Route path="/activeProduct" element={<AllActiveProducts/>} />
              <Route path="/product/:productId" element={<ProductViewRetrieveSingle/>} />
              <Route path='/addProduct' element={<AddProduct/>} />
              <Route path='/createOrder' element={<CreateOrder/>} />
             
              
              
              
             
              <Route path='/adminDashbord' element={<AdminDashbord/>} />
              <Route path='/retrieveAllProduct' element={<RetrieveAllProduct/>} />
              <Route path='/updateProduct/:id' element={<UpdateProduct/>} />
              <Route path='/archiveProduct/:id' element={<ArchiveProduct/>} />
             
           
              
              </Routes> 

        </Router>
      </UserProvider>
    </>
  );
}

export default App;
