import { useEffect, useState } from 'react';
import { Button, Space, Table, Image, message } from 'antd';
import Swal from 'sweetalert2'

const Dashboard = (product) => {

  const [filteredInfo, setFilteredInfo] = useState({});
  const [sortedInfo, setSortedInfo] = useState({});

  const handleChange = (pagination, filters, sorter) => {
    console.log('Various parameters', pagination, filters, sorter);
    setFilteredInfo(filters);
    setSortedInfo(sorter);
  };
  const columns = [
    {
      title: 'Name',
      dataIndex: 'name',
      key: 'name',
      sorter: (a, b) => a.name.localeCompare(b.name),
      sortOrder: sortedInfo.columnKey === 'name' ? sortedInfo.order : null,
      ellipsis: true,

    },
    {
      title: 'Description',
      dataIndex: 'description',
      key: 'description',
      sorter: (a, b) => a.description.localeCompare(b.description),
      sortOrder: sortedInfo.columnKey === 'description' ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: 'Price',
      dataIndex: 'price',
      key: 'price',
      sorter: (a, b) => a.price - b.price,
      sortOrder: sortedInfo.columnKey === 'price' ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: 'Color',
      dataIndex: 'color',
      key: 'color',
      sorter: (a, b) => a.color - b.color,
      sortOrder: sortedInfo.columnKey === 'color' ? sortedInfo.order : null,
      ellipsis: true,
    },
    {
      title: 'Status',
      dataIndex: 'isActive',
      key: 'isActive',
      render: (isActive) => {
        return (
          isActive ?
            <span style={{ color: 'green' }}><i className="fas fa-circle"></i> Active</span> :
            <span style={{ color: 'red' }}><i className="fas fa-circle"></i> Inactive</span>
        )
      }
    },
    {
      className: 'text-center',
      title: 'Actions',
      key: 'action',
      render: (text, record) => (
        <Space size="middle">
          {
            record.isActive
              ?
              <Button danger className='red-button' shape='round' onClick={() => archiveProduct(record._id)}>Archive</Button>
              :
              <Button className='green-button' shape='round' onClick={() => activateProduct(record._id)}>Activate</Button>
          }
          <Button type="primary" shape='round' href={`/updateProduct/${record._id}`}>Update</Button>
        </Space>
      ),
    },
  ];

  const [products, setProducts] = useState([])
  const [isLoading, setIsLoading] = useState(true);



  //  const [modeldata, setModeldata] = useState({
  //   name: '',
  //   description: '',
  //   price: '',
  //   color: '',
  //   isActive: ''
  // })

  useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/product/retrieveAllProducts`)
      .then(response => response.json())
      .then(res => {
        setProducts(res)
      })

  }, [])





  const archiveProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/product/archiveProduct/${id}`, {
      method: 'PUT',
      headers: {
        "Content-type": "application/json"
      },
      body: JSON.stringify({
        isActive: false
      })
    })

      .then(() => {

        setProducts(
          products.map((product) => {

            if (product._id === id) {
              return {
                ...product,
                isActive: false
              }
            }

            return product



          })
        )

        Swal.fire({
          title: 'Archive',
          icon: 'success',
          text: 'Successfully!'
        })


      })
  }

  const activateProduct = (id) => {
    fetch(`${process.env.REACT_APP_API_URL}/product/activateProduct/${id}`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        isActive: true,
      }),
    }).then(() => {
      // update the products array to reflect the updated state of the product
      setProducts(products.map((product) => {
        if (product._id === id) {
          return {
            ...product,
            isActive: true,
          };
        }
        // message.success(`${product.name} activated successfully`)
        console.log(product)
        return product;
      }));
      message.success("Product Activated!")
    });
  };

  return (

    <div class="container mt-5">
      <div class="row mt-5 ">
        <div class="col-lg-1 col-md-6 col-sm-12"/>
        
        <div class="col-lg-10 col-md-6 col-sm-12">
          <div class=" mt-2">
            <div className='tb-1'>
              <h3>Admin Dashbord</h3>
              <div className='btn-1'>
                <Button href='/addProduct'
                  className="border-0 px-1 py-1  fw-bold" variant="primary" type="submit" id="submitBtn">
                  Add New Products
                </Button>
                {/* <Button
                  className="border-0 px-1 py-1  fw-bold" variant="success" type="submit" id="submitBtn" >
                  Show User Orders
                </Button> */}


                <Table className='py-4' columns={columns} dataSource={products} onChange={handleChange} />


              </div>
            </div>
          </div>
        </div>
      </div>

    </div>




  )
}


export default Dashboard


