import {Card, CardImg, Col, Row, Button} from 'react-bootstrap'
import {Link} from 'react-router-dom'
import { useState } from 'react'



export default function ProductCard(product) {

    
       

    const { name, description, price, color, isActive, image, _id } = product.productProp
    



    return (
         <>
         
         <div className="card-container">
                <div className="card-box">
                     <Row className="mt-4 mb-3"> 
                        <Col xs={12} md={11}>
                            <Card>
                                 <Card.Body>
                                        <Card.Title>{name}</Card.Title>
                                        <Card.Subtitle>Description:</Card.Subtitle>
                                        <Card.Text>{description}</Card.Text>
                                        <Card.Subtitle>Color:</Card.Subtitle>
                                        <Card.Text>{color}</Card.Text>
                                        
                                        <Card.Subtitle>Price:</Card.Subtitle>
                                        <Card.Text>Php{price}</Card.Text>
                                        {/* <Card.Subtitle>Image</Card.Subtitle> */}
                                        {/* <CardImg top src={image}/> */}
                                        <Link className="btn btn-primary" to={`/product/${_id}`}>Details</Link>
                                          
                                    </Card.Body>
                            </Card>
                        </Col>
                    </Row> 
                   
                   
                 </div>     
         </div>   
         </>
  )
    
}